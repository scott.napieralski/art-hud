import Background from "@/app/Background";
import ClockClientWrapper from "@/app/ClockClientWrapper";
import Weather from "@/app/Weather";

export default async function Home() {

    const getImages = async () => {
        const NUM_IMAGES = 100;
        // TODO: add random page
        try {
            // get a list of artworks created after 1800
            const articSearchUrl = `https://api.artic.edu/api/v1/artworks/search?query[range][date_end][gte]=1800&limit=${NUM_IMAGES}&page=1&fields=id,artist_title,title,image_id,date_display`;
            const searchResponse = await fetch(articSearchUrl);
            if (searchResponse.ok) {
                const searchJson = await searchResponse.json();
                return searchJson.data;
            } else {
                console.error("Couldn't fetch search results." + searchResponse.statusText);
            }
        } catch (e) {
            console.error("Error while fetching search results", e);
        }
    }

    const images = await getImages();

    return (
        <Background images={images}>
            <ClockClientWrapper />
            <div style={{width: "100%", display: "flex", justifyContent: "flex-end"}}>
                <Weather />
            </div>
        </Background>
    );
}
