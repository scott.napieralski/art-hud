"use client";

import Clock from "react-live-clock";

export default function ClockClientWrapper() {
    return <Clock format="h:mm" style={{fontSize: "5em"}} />
}
