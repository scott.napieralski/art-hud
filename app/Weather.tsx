"use client";

import {useEffect, useState} from "react";
import styles from "./Weather.module.css";

interface OpenMeteoWeather {
    current_weather: {
        temperature: number,
        windspeed: number,
        winddirection: number,
        weathercode: number,
        is_day: number,
    },
    hourly: {
        time: Date[],
        temperature_2m: number[],
        precipitation_probability: number[],
        windspeed_10m: number[],
        winddirection_10m: number[],
    },
    daily: {
        time: Date[],
        sunrise: Date[],
        sunset: Date[]
    }
}

export default function Weather() {

    const [weather, setWeather] = useState<OpenMeteoWeather| undefined>(undefined);
    const [intervalId, setIntervalId] = useState(0);

    async function getWeatherJson(pos: GeolocationPosition) {
        const weatherUrl = `https://api.open-meteo.com/v1/forecast?latitude=${pos.coords.latitude}&longitude=${pos.coords.longitude}&hourly=temperature_2m,weathercode,precipitation_probability,windspeed_10m,winddirection_10m&daily=sunrise,sunset&current_weather=true&temperature_unit=fahrenheit&windspeed_unit=mph&precipitation_unit=inch&timezone=America%2FDenver&forecast_days=1`
        const weatherResponse = await fetch(weatherUrl);
        const weatherJson = await weatherResponse.json();
        setWeather(weatherJson);
    }

    function getWeatherIcon() {
        if(!weather) {
            return "not-available";
        }
        const weatherCode = weather.current_weather.weathercode;
        const isDaytime = weather.current_weather.is_day;

        const timeCode = isDaytime ? "-day" : "-night";
        switch (weatherCode) {
            case 0:
            case 1:
                return "clear" + timeCode;
            case 2:
                return "overcast" + timeCode;
            case 3:
                return "cloudy";
            case 45:
            case 48:
                return "fog";
            case 51:
                return `partly-cloudy${timeCode}-drizzle`;
            case 53:
            case 55:
                return "drizzle";
            case 56:
            case 57:
                return "sleet";
            case 61:
            case 80:
                return `partly-cloudy${timeCode}-rain`;
            case 63:
            case 81:
            case 65:
            case 82:
                return "rain";
            case 66:
            case 67:
                return "sleet";
            case 71:
            case 73:
            case 85:
                return `partly-cloudy${timeCode}-snow`
            case 75:
            case 77:
            case 86:
                return "snow";
            case 95:
            case 96:
            case 99:
                return "thunderstorms";
        }
    }

    useEffect(() => {
        const INTERVAL = 30 * 60 * 1000; // min * sec * millis
        function updateWeather() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((pos) => {
                    getWeatherJson(pos);
                }, (err) => {
                    // error.code can be:
                    //   0: unknown error
                    //   1: permission denied
                    //   2: position unavailable (error response from location provider)
                    //   3: timed out
                    console.error("Couldn't get location.", err);
                });
            }
        }

        updateWeather();
        if( !intervalId ) {
            const id = setInterval(updateWeather, INTERVAL);
            setIntervalId(id[Symbol.toPrimitive]);
        }
    }, []);

    const weatherIconName = getWeatherIcon();
    return <div className={styles.container}>
        <span className={styles.temperature}>{weather ? Math.round(weather.current_weather.temperature) : "?"}&deg;</span>
        <img className={styles.weatherIcon} src={`/meteocons/line/svg/${weatherIconName}.svg`} alt={weatherIconName} />
    </div>
}
