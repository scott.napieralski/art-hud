'use client'

import {PropsWithChildren, useEffect, useState} from "react";
import {ReactNodeArray} from "prop-types";
import styles from './Background.module.css';

interface ArticArtwork {
    image_id: string,
    title: string,
    artist_title: string,
    date_display: string
}

interface BackgroundProps {
    images: ArticArtwork[]
}

export default function Background(props: PropsWithChildren<BackgroundProps>) {

    function formatArticImgUrl(imgId: string) {
        return `https://www.artic.edu/iiif/2/${imgId}/full/1686,/0/default.jpg`;
    }
    const [imgInfo, setImgInfo] = useState<ArticArtwork | undefined>(undefined);
    const [intervalId, setIntervalId] = useState(0);

    useEffect(() => {
        const INTERVAL = 10 * 60 * 1000; // min * sec * millis
        function updateImage() {
            const index = Math.floor(Math.random() * props.images.length);
            // pre-fetch the images
            fetch(formatArticImgUrl(props.images[index].image_id), { mode: 'no-cors' }).then((res) => {
                setImgInfo(props.images[index]);
            });
        }

        updateImage();
        if( !intervalId ) {
            const id = setInterval(updateImage, INTERVAL);
            setIntervalId(id[Symbol.toPrimitive]);
        }
    }, [props.images]);

    if( !imgInfo ) {
        return <main>{props.children}</main>
    }

    return <main className={styles.grid} style={{backgroundImage: `url(${formatArticImgUrl(imgInfo.image_id)})`}}>
        {props.children}
        <div className={styles.artLabel}>
            <span>{imgInfo.artist_title}</span>
            <span style={{fontStyle: 'italic'}}>{imgInfo.title}</span>
            <span>{imgInfo.date_display}</span>
            <span>The Art Institute of Chicago</span>
        </div>
    </main>;
}
